# Aide pour le réglage de l'offset du BLTouch

## Liste des commandes

| Commande | Commentaire                                   |
|----------|-----------------------------------------------|
| M851 Z0  | Définition de l'offset Z à 0                  |
| M500     | Enregistrement                                |
| M211 S0  | Suppression protection déplacements négatifs  |
| G28      | Home all                                      |
| G1 Z0    | Aller à Z0                                    |
| M851 ZX  | Définition de l'offset Z sur la valeur voulue |
| M211 S1  | Action protection déplacements négatifs       |
| M500     | Enregistrement                                |
| G29      | Test de l'auto level                          |